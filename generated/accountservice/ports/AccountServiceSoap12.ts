import { SaveBundleList } from "../definitions/SaveBundleList";
import { SaveBundleListResponse } from "../definitions/SaveBundleListResponse";
import { GetEntryId } from "../definitions/GetEntryId";
import { GetEntryIdResponse } from "../definitions/GetEntryIdResponse";
import { GetAccountList } from "../definitions/GetAccountList";
import { GetAccountListResponse } from "../definitions/GetAccountListResponse";
import { GetTypeList } from "../definitions/GetTypeList";
import { GetTypeListResponse } from "../definitions/GetTypeListResponse";
import { GetTaxCodeList } from "../definitions/GetTaxCodeList";
import { GetTaxCodeListResponse } from "../definitions/GetTaxCodeListResponse";
import { CheckAccountNo } from "../definitions/CheckAccountNo";
import { CheckAccountNoResponse } from "../definitions/CheckAccountNoResponse";

export interface AccountServiceSoap12 {
    SaveBundleList(saveBundleList: SaveBundleList, callback: (err: any, result: SaveBundleListResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
    GetEntryId(getEntryId: GetEntryId, callback: (err: any, result: GetEntryIdResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
    GetAccountList(getAccountList: GetAccountList, callback: (err: any, result: GetAccountListResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
    GetTypeList(getTypeList: GetTypeList, callback: (err: any, result: GetTypeListResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
    GetTaxCodeList(getTaxCodeList: GetTaxCodeList, callback: (err: any, result: GetTaxCodeListResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
    CheckAccountNo(checkAccountNo: CheckAccountNo, callback: (err: any, result: CheckAccountNoResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
}
