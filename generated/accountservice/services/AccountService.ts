import { AccountServiceSoap } from "../ports/AccountServiceSoap";
import { AccountServiceSoap12 } from "../ports/AccountServiceSoap12";

export interface AccountService {
    readonly AccountServiceSoap: AccountServiceSoap;
    readonly AccountServiceSoap12: AccountServiceSoap12;
}
