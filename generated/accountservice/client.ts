import { Client as SoapClient, createClientAsync as soapCreateClientAsync } from "soap";
import { SaveBundleList } from "./definitions/SaveBundleList";
import { SaveBundleListResponse } from "./definitions/SaveBundleListResponse";
import { GetEntryId } from "./definitions/GetEntryId";
import { GetEntryIdResponse } from "./definitions/GetEntryIdResponse";
import { GetAccountList } from "./definitions/GetAccountList";
import { GetAccountListResponse } from "./definitions/GetAccountListResponse";
import { GetTypeList } from "./definitions/GetTypeList";
import { GetTypeListResponse } from "./definitions/GetTypeListResponse";
import { GetTaxCodeList } from "./definitions/GetTaxCodeList";
import { GetTaxCodeListResponse } from "./definitions/GetTaxCodeListResponse";
import { CheckAccountNo } from "./definitions/CheckAccountNo";
import { CheckAccountNoResponse } from "./definitions/CheckAccountNoResponse";
import { AccountService } from "./services/AccountService";

export interface AccountServiceClient extends SoapClient {
    AccountService: AccountService;
    SaveBundleListAsync(saveBundleList: SaveBundleList): Promise<[result: SaveBundleListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetEntryIdAsync(getEntryId: GetEntryId): Promise<[result: GetEntryIdResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetAccountListAsync(getAccountList: GetAccountList): Promise<[result: GetAccountListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetTypeListAsync(getTypeList: GetTypeList): Promise<[result: GetTypeListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetTaxCodeListAsync(getTaxCodeList: GetTaxCodeList): Promise<[result: GetTaxCodeListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    CheckAccountNoAsync(checkAccountNo: CheckAccountNo): Promise<[result: CheckAccountNoResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveBundleListAsync(saveBundleList: SaveBundleList): Promise<[result: SaveBundleListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetEntryIdAsync(getEntryId: GetEntryId): Promise<[result: GetEntryIdResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetAccountListAsync(getAccountList: GetAccountList): Promise<[result: GetAccountListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetTypeListAsync(getTypeList: GetTypeList): Promise<[result: GetTypeListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetTaxCodeListAsync(getTaxCodeList: GetTaxCodeList): Promise<[result: GetTaxCodeListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    CheckAccountNoAsync(checkAccountNo: CheckAccountNo): Promise<[result: CheckAccountNoResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
}

/** Create AccountServiceClient */
export function createClientAsync(...args: Parameters<typeof soapCreateClientAsync>): Promise<AccountServiceClient> {
    return soapCreateClientAsync(args[0], args[1], args[2]) as any;
}
