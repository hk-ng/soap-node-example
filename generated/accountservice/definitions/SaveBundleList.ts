import { Bundlelist } from "./Bundlelist";

/** SaveBundleList */
export interface SaveBundleList {
    /** bundlelist */
    bundlelist?: Bundlelist;
}
