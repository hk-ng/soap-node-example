import { LinkModel } from "./LinkModel";

/**
 * Links
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Links {
    /** LinkModel[] */
    LinkModel?: Array<LinkModel>;
}
