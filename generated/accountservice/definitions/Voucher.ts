import { Entries } from "./Entries";
import { DifferenceOptions } from "./DifferenceOptions";

/**
 * Voucher
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Voucher {
    /** s:int */
    TransactionNo?: string;
    /** Entries */
    Entries?: Entries;
    /** s:int */
    Sort?: string;
    /** DifferenceOptions */
    DifferenceOptions?: DifferenceOptions;
}
