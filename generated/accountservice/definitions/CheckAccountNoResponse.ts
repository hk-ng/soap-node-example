import { CheckAccountNoResult } from "./CheckAccountNoResult";

/** CheckAccountNoResponse */
export interface CheckAccountNoResponse {
    /** CheckAccountNoResult */
    CheckAccountNoResult?: CheckAccountNoResult;
}
