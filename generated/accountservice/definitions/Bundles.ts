import { Bundle } from "./Bundle";

/**
 * Bundles
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Bundles {
    /** Bundle[] */
    Bundle?: Array<Bundle>;
}
