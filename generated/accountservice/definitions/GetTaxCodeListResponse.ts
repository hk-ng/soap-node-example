import { GetTaxCodeListResult } from "./GetTaxCodeListResult";

/** GetTaxCodeListResponse */
export interface GetTaxCodeListResponse {
    /** GetTaxCodeListResult */
    GetTaxCodeListResult?: GetTaxCodeListResult;
}
