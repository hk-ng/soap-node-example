import { GetAccountListResult } from "./GetAccountListResult";

/** GetAccountListResponse */
export interface GetAccountListResponse {
    /** GetAccountListResult */
    GetAccountListResult?: GetAccountListResult;
}
