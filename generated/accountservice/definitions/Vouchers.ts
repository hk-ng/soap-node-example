import { Voucher } from "./Voucher";

/**
 * Vouchers
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Vouchers {
    /** Voucher[] */
    Voucher?: Array<Voucher>;
}
