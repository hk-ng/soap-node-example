import { TaxCodeElement } from "./TaxCodeElement";

/**
 * GetTaxCodeListResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface GetTaxCodeListResult {
    /** TaxCodeElement[] */
    TaxCodeElement?: Array<TaxCodeElement>;
}
