import { GetTypeListResult } from "./GetTypeListResult";

/** GetTypeListResponse */
export interface GetTypeListResponse {
    /** GetTypeListResult */
    GetTypeListResult?: GetTypeListResult;
}
