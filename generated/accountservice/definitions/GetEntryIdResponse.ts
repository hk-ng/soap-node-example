import { ArgEntryId } from "./ArgEntryId";

/** GetEntryIdResponse */
export interface GetEntryIdResponse {
    /** GetEntryIdResult */
    GetEntryIdResult?: ArgEntryId;
}
