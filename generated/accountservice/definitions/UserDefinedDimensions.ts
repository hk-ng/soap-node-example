import { UserDefinedDimension } from "./UserDefinedDimension";

/**
 * UserDefinedDimensions
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface UserDefinedDimensions {
    /** UserDefinedDimension[] */
    UserDefinedDimension?: Array<UserDefinedDimension>;
}
