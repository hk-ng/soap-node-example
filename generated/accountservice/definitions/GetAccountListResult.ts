import { AccountData } from "./AccountData";

/**
 * GetAccountListResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface GetAccountListResult {
    /** AccountData[] */
    AccountData?: Array<AccountData>;
}
