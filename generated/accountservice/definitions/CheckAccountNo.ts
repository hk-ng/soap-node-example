import { GetAccountListResult } from "./GetAccountListResult";

/** CheckAccountNo */
export interface CheckAccountNo {
    /** accountList */
    accountList?: GetAccountListResult;
}
