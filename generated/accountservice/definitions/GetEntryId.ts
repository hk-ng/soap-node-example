import { ArgEntryId } from "./ArgEntryId";

/** GetEntryId */
export interface GetEntryId {
    /** argEntryId */
    argEntryId?: ArgEntryId;
}
