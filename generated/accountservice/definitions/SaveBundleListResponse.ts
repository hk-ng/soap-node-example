import { SaveBundleListResult } from "./SaveBundleListResult";

/** SaveBundleListResponse */
export interface SaveBundleListResponse {
    /** SaveBundleListResult */
    SaveBundleListResult?: SaveBundleListResult;
}
