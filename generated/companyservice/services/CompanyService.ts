import { CompanyServiceSoap } from "../ports/CompanyServiceSoap";
import { CompanyServiceSoap12 } from "../ports/CompanyServiceSoap12";

export interface CompanyService {
    readonly CompanyServiceSoap: CompanyServiceSoap;
    readonly CompanyServiceSoap12: CompanyServiceSoap12;
}
