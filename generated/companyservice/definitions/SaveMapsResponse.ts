import { Maps } from "./Maps";

/** SaveMapsResponse */
export interface SaveMapsResponse {
    /** SaveMapsResult */
    SaveMapsResult?: Maps;
}
