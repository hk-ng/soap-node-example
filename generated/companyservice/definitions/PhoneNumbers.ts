import { Home } from "./Home";

/**
 * PhoneNumbers
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface PhoneNumbers {
    /** Home */
    Home?: Home;
    /** Fax */
    Fax?: Home;
    /** Mobile */
    Mobile?: Home;
    /** Primary */
    Primary?: Home;
    /** Work */
    Work?: Home;
}
