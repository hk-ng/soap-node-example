import { Category } from "./Category";

/**
 * GetCategoriesResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface GetCategoriesResult {
    /** Category[] */
    Category?: Array<Category>;
}
