import { Maps } from "./Maps";

/** DeleteMaps */
export interface DeleteMaps {
    /** maps */
    maps?: Maps;
}
