import { GroupMember } from "./GroupMember";

/**
 * Members
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Members {
    /** GroupMember[] */
    GroupMember?: Array<GroupMember>;
}
