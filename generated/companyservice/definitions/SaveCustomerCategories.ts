import { GetIndustriesResult } from "./GetIndustriesResult";

/** SaveCustomerCategories */
export interface SaveCustomerCategories {
    /** customerCategories */
    customerCategories?: GetIndustriesResult;
}
