import { Companies } from "./Companies";

/** SaveCompaniesResponse */
export interface SaveCompaniesResponse {
    /** SaveCompaniesResult */
    SaveCompaniesResult?: Companies;
}
