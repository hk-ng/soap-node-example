import { GetCategoriesResult } from "./GetCategoriesResult";

/** SaveCategoriesResponse */
export interface SaveCategoriesResponse {
    /** SaveCategoriesResult */
    SaveCategoriesResult?: GetCategoriesResult;
}
