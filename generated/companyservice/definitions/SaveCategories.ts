import { GetCategoriesResult } from "./GetCategoriesResult";

/** SaveCategories */
export interface SaveCategories {
    /** categories */
    categories?: GetCategoriesResult;
}
