import { Maps } from "./Maps";

/** DeleteMapsResponse */
export interface DeleteMapsResponse {
    /** DeleteMapsResult */
    DeleteMapsResult?: Maps;
}
