import { GetIndustriesResult } from "./GetIndustriesResult";

/** GetIndustriesResponse */
export interface GetIndustriesResponse {
    /** GetIndustriesResult */
    GetIndustriesResult?: GetIndustriesResult;
}
