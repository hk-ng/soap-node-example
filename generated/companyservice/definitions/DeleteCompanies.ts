import { Companies } from "./Companies";

/** DeleteCompanies */
export interface DeleteCompanies {
    /** companies */
    companies?: Companies;
}
