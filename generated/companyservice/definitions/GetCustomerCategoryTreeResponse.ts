import { GetIndustriesResult } from "./GetIndustriesResult";

/** GetCustomerCategoryTreeResponse */
export interface GetCustomerCategoryTreeResponse {
    /** GetCustomerCategoryTreeResult */
    GetCustomerCategoryTreeResult?: GetIndustriesResult;
}
