import { Relations } from "./Relations";

/** SaveRelations */
export interface SaveRelations {
    /** relations */
    relations?: Relations;
}
