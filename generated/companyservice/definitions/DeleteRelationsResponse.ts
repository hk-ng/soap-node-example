import { Relations } from "./Relations";

/** DeleteRelationsResponse */
export interface DeleteRelationsResponse {
    /** DeleteRelationsResult */
    DeleteRelationsResult?: Relations;
}
