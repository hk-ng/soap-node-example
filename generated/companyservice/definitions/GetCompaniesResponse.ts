import { Companies } from "./Companies";

/** GetCompaniesResponse */
export interface GetCompaniesResponse {
    /** GetCompaniesResult */
    GetCompaniesResult?: Companies;
}
