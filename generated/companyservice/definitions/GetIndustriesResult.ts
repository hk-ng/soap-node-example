import { KeyValuePair } from "./KeyValuePair";

/**
 * GetIndustriesResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface GetIndustriesResult {
    /** KeyValuePair[] */
    KeyValuePair?: Array<KeyValuePair>;
}
