import { GetIndustriesResult } from "./GetIndustriesResult";

/** DeleteCustomerCategories */
export interface DeleteCustomerCategories {
    /** customerCategories */
    customerCategories?: GetIndustriesResult;
}
