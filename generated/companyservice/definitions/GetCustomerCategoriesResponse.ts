import { CompanyIds } from "./CompanyIds";

/** GetCustomerCategoriesResponse */
export interface GetCustomerCategoriesResponse {
    /** GetCustomerCategoriesResult */
    GetCustomerCategoriesResult?: CompanyIds;
}
