import { SyncCompany } from "./SyncCompany";

/**
 * Items
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Items {
    /** SyncCompany[] */
    SyncCompany?: Array<SyncCompany>;
}
