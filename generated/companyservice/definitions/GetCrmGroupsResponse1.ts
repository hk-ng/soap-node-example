import { GetCrmGroupsResult } from "./GetCrmGroupsResult";

/** GetCRMGroupsResponse */
export interface GetCrmGroupsResponse1 {
    /** GetCRMGroupsResult */
    GetCRMGroupsResult?: GetCrmGroupsResult;
}
