import { GetIndustriesResult } from "./GetIndustriesResult";

/** GetStatusesResponse */
export interface GetStatusesResponse {
    /** GetStatusesResult */
    GetStatusesResult?: GetIndustriesResult;
}
