import { GetCategoriesResult } from "./GetCategoriesResult";

/** GetCategoriesResponse */
export interface GetCategoriesResponse {
    /** GetCategoriesResult */
    GetCategoriesResult?: GetCategoriesResult;
}
