import { SaveCustomerCategoriesResult } from "./SaveCustomerCategoriesResult";

/** DeleteCustomerCategoriesResponse */
export interface DeleteCustomerCategoriesResponse {
    /** DeleteCustomerCategoriesResult */
    DeleteCustomerCategoriesResult?: SaveCustomerCategoriesResult;
}
