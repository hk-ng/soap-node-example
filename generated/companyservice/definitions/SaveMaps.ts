import { Maps } from "./Maps";

/** SaveMaps */
export interface SaveMaps {
    /** maps */
    maps?: Maps;
}
