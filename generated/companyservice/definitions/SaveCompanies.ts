import { Companies } from "./Companies";

/** SaveCompanies */
export interface SaveCompanies {
    /** companies */
    companies?: Companies;
}
