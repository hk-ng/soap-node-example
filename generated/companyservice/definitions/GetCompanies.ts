import { SearchParams } from "./SearchParams";
import { ReturnProperties } from "./ReturnProperties";

/** GetCompanies */
export interface GetCompanies {
    /** searchParams */
    searchParams?: SearchParams;
    /** returnProperties */
    returnProperties?: ReturnProperties;
}
