import { SyncSearchParameters } from "./SyncSearchParameters";

/** GetCompanySyncList */
export interface GetCompanySyncList {
    /** syncSearchParameters */
    syncSearchParameters?: SyncSearchParameters;
}
