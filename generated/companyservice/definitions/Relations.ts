import { Relation } from "./Relation";

/**
 * relations
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Relations {
    /** Relation[] */
    Relation?: Array<Relation>;
}
