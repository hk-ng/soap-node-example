import { Relations } from "./Relations";

/** DeleteRelations */
export interface DeleteRelations {
    /** relations */
    relations?: Relations;
}
