import { Companies } from "./Companies";

/** DeleteCompaniesResponse */
export interface DeleteCompaniesResponse {
    /** DeleteCompaniesResult */
    DeleteCompaniesResult?: Companies;
}
