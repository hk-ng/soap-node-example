import { GetCompanySyncListResult } from "./GetCompanySyncListResult";

/** GetCompanySyncListResponse */
export interface GetCompanySyncListResponse {
    /** GetCompanySyncListResult */
    GetCompanySyncListResult?: GetCompanySyncListResult;
}
