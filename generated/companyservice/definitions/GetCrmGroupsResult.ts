import { Group } from "./Group";

/**
 * GetCRMGroupsResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface GetCrmGroupsResult {
    /** Group[] */
    Group?: Array<Group>;
}
