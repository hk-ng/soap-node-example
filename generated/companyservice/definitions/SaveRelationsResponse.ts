import { Relations } from "./Relations";

/** SaveRelationsResponse */
export interface SaveRelationsResponse {
    /** SaveRelationsResult */
    SaveRelationsResult?: Relations;
}
