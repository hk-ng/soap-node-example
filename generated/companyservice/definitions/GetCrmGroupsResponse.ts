import { GetCrmGroupsResult } from "./GetCrmGroupsResult";

/** GetCRMGroupsResponse */
export interface GetCrmGroupsResponse {
    /** GetCRMGroupsResult */
    GetCRMGroupsResult?: GetCrmGroupsResult;
}
