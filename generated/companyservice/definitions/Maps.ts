import { CompanyMap } from "./CompanyMap";

/**
 * Maps
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface Maps {
    /** CompanyMap[] */
    CompanyMap?: Array<CompanyMap>;
}
