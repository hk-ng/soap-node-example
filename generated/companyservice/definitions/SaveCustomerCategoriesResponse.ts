import { SaveCustomerCategoriesResult } from "./SaveCustomerCategoriesResult";

/** SaveCustomerCategoriesResponse */
export interface SaveCustomerCategoriesResponse {
    /** SaveCustomerCategoriesResult */
    SaveCustomerCategoriesResult?: SaveCustomerCategoriesResult;
}
