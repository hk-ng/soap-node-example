import { ApiException } from "./ApiException";

/**
 * SaveCustomerCategoriesResult
 * @targetNSAlias `tns`
 * @targetNamespace `http://24sevenOffice.com/webservices`
 */
export interface SaveCustomerCategoriesResult {
    /** APIException[] */
    APIException?: Array<ApiException>;
}
