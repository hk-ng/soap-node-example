import { Client as SoapClient, createClientAsync as soapCreateClientAsync } from "soap";
import { DeleteRelations } from "./definitions/DeleteRelations";
import { DeleteRelationsResponse } from "./definitions/DeleteRelationsResponse";
import { DeleteCompanies } from "./definitions/DeleteCompanies";
import { DeleteCompaniesResponse } from "./definitions/DeleteCompaniesResponse";
import { SaveRelations } from "./definitions/SaveRelations";
import { SaveRelationsResponse } from "./definitions/SaveRelationsResponse";
import { DeleteMaps } from "./definitions/DeleteMaps";
import { DeleteMapsResponse } from "./definitions/DeleteMapsResponse";
import { SaveMaps } from "./definitions/SaveMaps";
import { SaveMapsResponse } from "./definitions/SaveMapsResponse";
import { GetCompanySyncList } from "./definitions/GetCompanySyncList";
import { GetCompanySyncListResponse } from "./definitions/GetCompanySyncListResponse";
import { GetCompanies } from "./definitions/GetCompanies";
import { GetCompaniesResponse } from "./definitions/GetCompaniesResponse";
import { GetCategories } from "./definitions/GetCategories";
import { GetCategoriesResponse } from "./definitions/GetCategoriesResponse";
import { SaveCategories } from "./definitions/SaveCategories";
import { SaveCategoriesResponse } from "./definitions/SaveCategoriesResponse";
import { GetCustomerCategories } from "./definitions/GetCustomerCategories";
import { GetCustomerCategoriesResponse } from "./definitions/GetCustomerCategoriesResponse";
import { GetIndustries } from "./definitions/GetIndustries";
import { GetIndustriesResponse } from "./definitions/GetIndustriesResponse";
import { SaveCustomerCategories } from "./definitions/SaveCustomerCategories";
import { SaveCustomerCategoriesResponse } from "./definitions/SaveCustomerCategoriesResponse";
import { DeleteCustomerCategories } from "./definitions/DeleteCustomerCategories";
import { DeleteCustomerCategoriesResponse } from "./definitions/DeleteCustomerCategoriesResponse";
import { GetCustomerCategoryTree } from "./definitions/GetCustomerCategoryTree";
import { GetCustomerCategoryTreeResponse } from "./definitions/GetCustomerCategoryTreeResponse";
import { SaveCompanies } from "./definitions/SaveCompanies";
import { SaveCompaniesResponse } from "./definitions/SaveCompaniesResponse";
import { GetStatuses } from "./definitions/GetStatuses";
import { GetStatusesResponse } from "./definitions/GetStatusesResponse";
import { GetCrmGroups } from "./definitions/GetCrmGroups";
import { GetCrmGroupsResponse } from "./definitions/GetCrmGroupsResponse";
import { GetCrmGroups1 } from "./definitions/GetCrmGroups1";
import { GetCrmGroupsResponse1 } from "./definitions/GetCrmGroupsResponse1";
import { CompanyService } from "./services/CompanyService";

export interface CompanyServiceClient extends SoapClient {
    CompanyService: CompanyService;
    DeleteRelationsAsync(deleteRelations: DeleteRelations): Promise<[result: DeleteRelationsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteCompaniesAsync(deleteCompanies: DeleteCompanies): Promise<[result: DeleteCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveRelationsAsync(saveRelations: SaveRelations): Promise<[result: SaveRelationsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteMapsAsync(deleteMaps: DeleteMaps): Promise<[result: DeleteMapsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveMapsAsync(saveMaps: SaveMaps): Promise<[result: SaveMapsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCompanySyncListAsync(getCompanySyncList: GetCompanySyncList): Promise<[result: GetCompanySyncListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCompaniesAsync(getCompanies: GetCompanies): Promise<[result: GetCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCategoriesAsync(getCategories: GetCategories): Promise<[result: GetCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCategoriesAsync(saveCategories: SaveCategories): Promise<[result: SaveCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCustomerCategoriesAsync(getCustomerCategories: GetCustomerCategories): Promise<[result: GetCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetIndustriesAsync(getIndustries: GetIndustries): Promise<[result: GetIndustriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCustomerCategoriesAsync(saveCustomerCategories: SaveCustomerCategories): Promise<[result: SaveCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteCustomerCategoriesAsync(deleteCustomerCategories: DeleteCustomerCategories): Promise<[result: DeleteCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCustomerCategoryTreeAsync(getCustomerCategoryTree: GetCustomerCategoryTree): Promise<[result: GetCustomerCategoryTreeResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCompaniesAsync(saveCompanies: SaveCompanies): Promise<[result: SaveCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetStatusesAsync(getStatuses: GetStatuses): Promise<[result: GetStatusesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCRMGroupsAsync(getCrmGroups: GetCrmGroups): Promise<[result: GetCrmGroupsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteRelationsAsync(deleteRelations: DeleteRelations): Promise<[result: DeleteRelationsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteCompaniesAsync(deleteCompanies: DeleteCompanies): Promise<[result: DeleteCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveRelationsAsync(saveRelations: SaveRelations): Promise<[result: SaveRelationsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteMapsAsync(deleteMaps: DeleteMaps): Promise<[result: DeleteMapsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveMapsAsync(saveMaps: SaveMaps): Promise<[result: SaveMapsResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCompanySyncListAsync(getCompanySyncList: GetCompanySyncList): Promise<[result: GetCompanySyncListResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCompaniesAsync(getCompanies: GetCompanies): Promise<[result: GetCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCategoriesAsync(getCategories: GetCategories): Promise<[result: GetCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCategoriesAsync(saveCategories: SaveCategories): Promise<[result: SaveCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCustomerCategoriesAsync(getCustomerCategories: GetCustomerCategories): Promise<[result: GetCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetIndustriesAsync(getIndustries: GetIndustries): Promise<[result: GetIndustriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCustomerCategoriesAsync(saveCustomerCategories: SaveCustomerCategories): Promise<[result: SaveCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    DeleteCustomerCategoriesAsync(deleteCustomerCategories: DeleteCustomerCategories): Promise<[result: DeleteCustomerCategoriesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCustomerCategoryTreeAsync(getCustomerCategoryTree: GetCustomerCategoryTree): Promise<[result: GetCustomerCategoryTreeResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    SaveCompaniesAsync(saveCompanies: SaveCompanies): Promise<[result: SaveCompaniesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetStatusesAsync(getStatuses: GetStatuses): Promise<[result: GetStatusesResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
    GetCRMGroupsAsync(getCrmGroups: GetCrmGroups1): Promise<[result: GetCrmGroupsResponse1, rawResponse: any, soapHeader: any, rawRequest: any]>;
}

/** Create CompanyServiceClient */
export function createClientAsync(...args: Parameters<typeof soapCreateClientAsync>): Promise<CompanyServiceClient> {
    return soapCreateClientAsync(args[0], args[1], args[2]) as any;
}
