import path from "path";
import fs from "fs/promises";
import { createClientAsync as createAccountClient } from "./generated/accountservice";
import { createClientAsync as createCompanyClient } from "./generated/companyservice";
import { WSDL } from "soap";

(async function () {
  const accountClient = await createAccountClient(
    path.resolve("./resources/AccountService.wsdl")
  );
  const companyClient = await createCompanyClient(
    path.resolve("./resources/AccountService.wsdl")
  );
  console.log(path.resolve("./resources/AccountService.wsdl"));
//   const wsdl = new WSDL(
//     await fs.readFile(path.resolve("./resources/AccountService.wsdl"), "utf-8"),
//     "http://localhost",
//     {}
//   );
//   wsdl.onReady(() => {
//     console.log(
//       wsdl.objectToXML(
//         {
//           ContractVersion: "0",
//         },
//         "",
//         "h",
//         "http://schemas.navitaire.com/WebServices",
//         true
//       )
//     );
//   });

  accountClient.addSoapHeader(
    {
      ContractVersion: "0",
      EnableExceptionStackTrace: false,
      MessageContractVersion: "version",
      Signature: "Signature"
    },
    "",
    "h",
    "http://schemas.navitaire.com/WebServices"
  );
  accountClient.GetEntryIdAsync({
    argEntryId: { Date: "", EntryNo: "", SortNo: "" },
  });
})();
